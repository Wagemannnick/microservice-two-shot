import React, { Component } from 'react';

class ShoeForm extends Component {
    constructor(props){
        super(props);
        this.state={
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins:[], 
        };
        this.handleManufacturerChange=this.handleManufacturerChange.bind(this);
        this.handleModelNameChange=this.handleModelNameChange.bind(this);
        this.handleColorChange=this.handleColorChange.bind(this);
        this.handlePictureUrlChange=this.handlePictureUrlChange.bind(this);
        this.handleBinChange=this.handleBinChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }
    async componentDidMount(){
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json()
            this.setState({bins: data.bins})

        }

    }
    handleManufacturerChange(event){
        const value = event.target.value;
        this.setState({manufacturer:value})
    }
    handleModelNameChange(event){
        const value = event.target.value;
        this.setState({model_name:value})
    }
    handleColorChange(event){
        const value = event.target.value;
        this.setState({color:value})
    }
    handlePictureUrlChange(event){
        const value = event.target.value;
        this.setState({picture_url:value})
    }
    handleBinChange(event){
        const value = event.target.value;
        this.setState({bin:value})
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state}
        delete data.bins
        console.log("CHeck this--->", data)

        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok){
            const newShoe = await response.json()
            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
                }
            this.setState(cleared)
        } 
    }
    render(){
        return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Manufacturer" type="text" className="form-control" id="manufacturer" name="manufacturer" value={this.state.manufacturer} onChange={this.handleManufacturerChange} />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Model Name" type="text" className="form-control" id="name" name="name" value={this.state.model_name} onChange={this.handleModelNameChange} />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Color" type="text" className="form-control" id="color" name="color" value={this.state.color} onChange={this.handleColorChange} />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Picture URL" type="text" className="form-control" id="picture_url" name="picture_url" value={this.state.picture_url} onChange={this.handlePictureUrlChange} />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleBinChange} value={this.state.bin} name="bin" required id="bin" className="form-select">
                  <option value="">Bin</option>
                  {this.state.bins.map(bin => {
                    return <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                  })}
                </select>
              </div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
        )
    }
}
export default ShoeForm