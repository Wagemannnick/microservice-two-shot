from django.contrib import admin
from .models import Hat, LocationVo


class HatAdmin(admin.ModelAdmin):
    pass

class LocationVoAdmin(admin.ModelAdmin):
    pass



admin.site.register(LocationVo, LocationVoAdmin)
admin.site.register(Hat, HatAdmin)
