import React, { Component } from 'react';

class HatList extends Component {
    constructor(props){
        super(props)
        this.state = {hats: props.hats}
    }
    async deleteHat(hat) {
        const deleteUrl = `http://localhost:8090/api/hats/${hat}/`
        const fetchConfig = {
            method: "delete",
        }
        const data = await fetch(deleteUrl, fetchConfig)
        const response = await fetch('http://localhost:8090/api/hats/')
        if (response.ok){
            const data2 = await response.json()
            const list= []
            for (let item of data2.hats){
                list.push(item)
            }
            this.setState = {hats:list}
        }
        this.setState = {hats: this.state.hats}
    }

    render(){
    return(
        <table>
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location Name</th>
                </tr>
            </thead>
            <tbody>
                {this.state.hats.map(hat =>{
                    return (
                        <tr key={hat.id}>
                            <td><img src={hat.picture_url} alt=""/></td>
                            <td>{hat.style}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>{hat.id}</td>
                            <button onClick={() => this.deleteHat(hat.id)}>Delete</button>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
    }
}
export default HatList;
