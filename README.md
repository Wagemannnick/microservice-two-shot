# Wardrobify

Team:

* Teresa Tan - Shoes & Hats
* Nick Wagemann - Hats & Shoes

## Design
1. From wardrobe home page, there will be 2 links
    - Shoes
    - Hats
2. From the shoe page, there will be a list of all the bins
3. When user clicks on a bin, they'll be brought to a list of all shoes in that bin only
4. Clicking on a specific shoe brings up more details about the shoe, as well as a
delete option and a drop-down list to move the shoe to a different bin




## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
