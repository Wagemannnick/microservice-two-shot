from xmlrpc.client import ResponseError
from django.shortcuts import render
from django.http import JsonResponse
from .models import Hat, LocationVo
from common.json import ModelEncoder
import json



class LocationVoDetailEncoder(ModelEncoder):
    model = LocationVo
    properties = [
        "closet_name",
        "section_number",
        "href"
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "id"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVoDetailEncoder(),
    }

def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["location"]
            location = LocationVo.objects.get(href=href)
            content["location"] = location
        except LocationVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location Id"},
                status = 400,
            )
    
    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatDetailEncoder,
        safe=False,
    )

def api_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try: 
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                current_location = LocationVo.objects.get(href=content["location"])
                content["location"] = current_location
        except LocationVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location VO"},
                status=400,
                )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )