import React, { Component } from 'react';

class HatForm extends Component {
    constructor(props){
        super(props);
        this.state={
            style: '',
            fabric: '',
            color: '',
            picture_url: '',
            // location: '',
            locations: []
        };
        this.handleStyleChange=this.handleStyleChange.bind(this);
        this.handleFabricChange=this.handleFabricChange.bind(this);
        this.handleColorChange=this.handleColorChange.bind(this);
        this.handlePictureUrlChange=this.handlePictureUrlChange.bind(this);
        this.handleLocationChange=this.handleLocationChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    async componentDidMount(){
        const url = "http://localhost:8100/api/locations/"
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json()
            this.setState({locations: data.locations})

        }

    }
    handleStyleChange(event){
        const value = event.target.value;
        this.setState({style:value})
    }
    handleFabricChange(event){
        const value = event.target.value;
        this.setState({fabric:value})
    }
    handleColorChange(event){
        const value = event.target.value;
        this.setState({color:value})
    }
    handlePictureUrlChange(event){
        const value = event.target.value;
        this.setState({picture_url:value})
    }
    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location:value})
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state}
        delete data.locations

        const hatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok){
            const newHat = await response.json()
            const cleared = {
                style: '',
                fabric: '',
                color: '',
                picture_url: '',
                location: '',
                }
            this.setState(cleared)
        } 
    }
    render(){
        return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Style" type="text" className="form-control" id="style" name="style" value={this.state.style} onChange={this.handleStyleChange} />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Fabric" type="text" className="form-control" id="fabric" name="fabric" value={this.state.fabric} onChange={this.handleFabricChange} />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Color" type="text" className="form-control" id="color" name="color" value={this.state.color} onChange={this.handleColorChange} />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Picture URL" type="text" className="form-control" id="picture_url" name="picture_url" value={this.state.picture_url} onChange={this.handlePictureUrlChange} />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} name="location" required id="location" className="form-select">
                  <option value="">Location</option>
                  {this.state.locations.map(location => {
                    return <option key={location.href} value={location.href}>{location.closet_name}</option>
                  })}
                </select>
              </div>
              <button type="submit" className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
        )
    }
}
export default HatForm;