# Generated by Django 4.0.3 on 2022-07-27 22:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model_name', models.CharField(max_length=255)),
                ('manufacturer', models.CharField(max_length=255)),
                ('color', models.CharField(max_length=255)),
                ('picture_url', models.URLField(null=True)),
            ],
        ),
    ]
