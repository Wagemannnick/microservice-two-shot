# Generated by Django 4.0.3 on 2022-07-27 23:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_shoevo'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ShoeVO',
            new_name='BinVO',
        ),
    ]
