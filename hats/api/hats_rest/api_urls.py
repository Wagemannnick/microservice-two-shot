from django.urls import path

from .api_views import api_list_hats, api_hat

urlpatterns = [
    # path("locations/", api_list_hats, name="api_list_hats"),
    # path("locations/<int:pk>/", api_hat, name="api_hat"),
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_hat, name="api_hats"),
]