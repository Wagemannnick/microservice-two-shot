import React, { Component } from 'react';



class ShoeList extends Component {
    constructor(props) {
        super(props)
        this.state = { shoes: props.shoes}
        // console.log("!!!!!!!!!!", this.state)
    }

    // async componentDidMount() {
    
        
    async deleteShoe(shoe) {
        // event.preventDefault();
        const deleteUrl = `http://localhost:8080/api/shoes/${shoe}/`;
        // console.log("!!!!!!!!!!!!", deleteUrl)
        const fetchConfig = {
            method: "delete",
        }
        const data = await fetch(deleteUrl, fetchConfig)
        const response = await fetch('http://localhost:8080/api/shoes/')
        if (response.ok) {
            const data2 = await response.json()
            const list = []
            for (let item of data2.shoes) {
                console.log("!!!!", item)
                list.push(item)
            }
            this.setState = {shoes: list}
            console.log("----->", this.state)
        }
    
        this.setState = { shoes: this.state.shoes}
    }

    render() {
    return (
        <table>
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Bin Name</th>
                </tr>
            </thead>
            <tbody>
                {this.state.shoes.map(shoe =>{
                    console.log("Picture--->", shoe.picture_url)
                    return (
                        <tr key={shoe.id}>
                            {console.log(shoe)}
                            <td><img src={shoe.picture_url} alt=""/></td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.id}</td>
                            <button onClick={() => this.deleteShoe(shoe.id)}>Delete</button>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
    }
}
export default ShoeList;