from django.db import models


class BinVO(models.Model):
    closet_name = models.CharField(max_length=255)
    bin_number = models.PositiveIntegerField()
    href = models.CharField(max_length=255, unique=True, null=True)


class Shoe(models.Model):
    model_name = models.CharField(max_length=255)
    manufacturer = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.PROTECT,
        null=True)

