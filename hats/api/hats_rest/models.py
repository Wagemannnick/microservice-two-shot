from django.db import models


class LocationVo(models.Model):
    closet_name = models.CharField(max_length=255)
    section_number = models.PositiveIntegerField()
    href = models.CharField(max_length=255, unique=True, null=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=255)
    style = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVo,
        related_name="location",
        on_delete=models.PROTECT,
        null=True)

