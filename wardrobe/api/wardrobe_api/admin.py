from django.contrib import admin
from .models import Bin




class BinAdmin(admin.ModelAdmin):
    pass

admin.site.register(Bin, BinAdmin)
