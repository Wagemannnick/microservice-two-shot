from django.contrib import admin
from .models import Shoe


class ShoeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Shoe, ShoeAdmin)

