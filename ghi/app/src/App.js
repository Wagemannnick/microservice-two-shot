import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import HatList from './HatList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';

function App(props) {
  console.log("look at me--->", props)
  if (props.shoes===undefined){
    return null;
  }
  if (props.hats===undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="shoes/create" element={<ShoeForm />} />
          <Route path="/hats" element={<HatList hats={props.hats} />} />
          <Route path="hats/create" element={<HatForm />} />
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
          
