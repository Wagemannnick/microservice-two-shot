import django
import os
import sys
import time
import json
import requests



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVo


def get_location():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    try:
        for location in content["locations"]:
            LocationVo.objects.update_or_create(
                section_number=location["section_number"],
                defaults ={
                    "closet_name": location["closet_name"],
                    "href": location["href"],
                    "section_number": location["section_number"]
                }
            )
    except Exception as e:
        print(e, file=sys.stderr)

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_location()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
