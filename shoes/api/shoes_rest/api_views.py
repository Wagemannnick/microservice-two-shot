from django.shortcuts import render
from django.http import JsonResponse
from .models import BinVO, Shoe
from common.json import ModelEncoder
import json



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "href"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id"
    ]



class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["bin"]
            bin = BinVO.objects.get(href=href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        
    shoe = Shoe.objects.create(**content)
    return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )

def api_shoe(request,pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE": 
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            ) 
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )
    else:
        content=json.loads(request.body)
        try:
            if "bin" in content:
                current_bin = BinVO.objects.get(href=content["bin"])
                content["bin"] = current_bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Vo"},
                status=400
                )
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
            # shoe=Shoe.objects.get(id=pk)
            # shoes=[
            #     "model_name",
            #     "manufacture",
            #     "color",
            #     "picture_url",
            #     "bin"
            # ]
            # for shoe in shoes:
            #     if shoe in content:
            #         print("Look here---->", content["shoe"])
            #         setattr(shoe,shoes,content["shoe"])
            # shoe.save()
            # return JsonResponse(
            #     shoe,
            #     encoder=ShoeDetailEncoder,
            #     safe=False
            # )