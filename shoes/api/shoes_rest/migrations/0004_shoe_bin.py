# Generated by Django 4.0.3 on 2022-07-27 23:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_rename_shoevo_binvo'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='bin', to='shoes_rest.binvo'),
        ),
    ]
